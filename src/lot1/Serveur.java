package lot1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Serveur {
	
	public static final int TENTATIVE_MAX = 4;

	public static void main(String[] args) throws IOException {

		// String sous forme de tableau de bytes pour le write
		byte[] more = "\r\n+\r\n".getBytes();
		byte[] less = "\r\n-\r\n".getBytes();
		byte[] connexion = "Connexion r�ussie\r\n".getBytes();
		int tentative = 1;

		// creer un nombre au pif entre 1 et 100
		Random rand = new Random();
		Integer numberToFind = rand.nextInt(100 - 1) + 1;
		System.out.println("nombre � trouver : " + numberToFind);

		// en attente d'utilisateur
		ServerSocket s = new ServerSocket(8080);
		Socket c = new Socket();
		c = s.accept();
		OutputStream os = c.getOutputStream();
		InputStream is = c.getInputStream();

		// connexion utilisateur
		os.write(connexion);

		// reception de son nom
		String name = getResponse(is);
		os.write(("\r\nBienvenu " + name + ", choisissez un nombre entre 0 et 100\r\n").getBytes());

		boolean game = true;

		// boucle d'essais
		do {
			Integer res = new Integer(new String(getResponse(is)));
			System.out.println("r�sultat : " + res);
			if (numberToFind > res) {
				os.write(more);
			} else if (numberToFind.equals(res)) {
				game = false;
				os.write(("\r\nBravo " + name).getBytes());
			} else {
				os.write(less);
			}
			if(tentative >= TENTATIVE_MAX) {
				os.write(("\r\nPerdu " + name).getBytes());
				break;
			}
			tentative++;

		} while (game);
		c.close();
		s.close();

	}

	// methode de lecture reponse utilisateur
	private static String getResponse(InputStream is) throws IOException {
		byte[] rep = new byte[255];
		int num = is.read();
		int index = 0;
		while (num != 13) {
			rep[index] = (byte) num;
			index++;
			num = is.read();
		}
		return new String(rep).trim();
	}

}